﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Lab_2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        
        public Page1(string Name, string Text, string source)
        {
            InitializeComponent();

            ShowName.Text = Name;

            ImageCall.Source = new UriImageSource()
            {
                Uri = new Uri(source)
            };
   
           
             
        }

    }
}