﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace CS481_Lab_2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
       
        public ObservableCollection<ListViewItems> Items { get; set; } 
        
        public MainPage()
        {
            InitializeComponent();
            List();
            
        }

        void List()
        {
            Items = new ObservableCollection<ListViewItems>();

            // Creating list items
            Items.Add(new ListViewItems
            {
                Name = "California States San Marcos",
                Image_1 = "https://upload.wikimedia.org/wikipedia/commons/4/42/Cal_State_San_Marcos.jpg",
                Image_2 = "https://upload.wikimedia.org/wikipedia/commons/6/69/Official_CSUSM_Logo.jpg",

                Text = "Welcome to CSUSM"
            }) ;
            Items.Add(new ListViewItems
            {
                Name = "University of California San Diego",
                Image_1 = "https://upload.wikimedia.org/wikipedia/commons/4/4c/Price_Center%2C_UCSD.jpg",
                Image_2 = "https://philosophy.ucsd.edu/_images/people/triton-no-photo.jpg",
                Text = "Welcome to UCSD"
            });

            Items.Add(new ListViewItems
            {
                Name = "San Diego State University",
                Image_1 = "https://upload.wikimedia.org/wikipedia/commons/3/3a/Courtyard_SDSU.jpg",
                Image_2 = "https://brand.sdsu.edu/primary-logos/SDSUprimary3Crgb.jpg",
                Text = "Welcome to SDSU"
            });

            Items.Add(new ListViewItems
            {
                Name = "University of Los Angeles",
                Image_1 = "https://upload.wikimedia.org/wikipedia/commons/3/39/Janss_Steps%2C_Royce_Hall_in_background%2C_UCLA.jpg",
                Image_2 = "https://www.smdp.com/wp-content/uploads/2017/03/18727184_1489077485.0079_funddescription.png",
                Text = "Welcome to UCLA"
            });

            //Tim's cellst
            Items.Add(new ListViewItems
            {
                Name = "UC Davis (Tim's Added Cell)",
                Image_1 = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/The_University_of_California_Davis.svg/1200px-The_University_of_California_Davis.svg.png",
                Image_2 = "https://its.ucdavis.edu/wp-content/uploads/UC-Davis-West-Village-David-Lloyd-800x500.jpg",
                Text = "Welcome to UC Davis (From Tim's branch)"
            });

            // I added new list - 3/10 . 
            Items.Add(new ListViewItems
            {
                Name = "New York University",
                Image_1 = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSIzQqLiroiWDa0xF8qQ58cFhRzaCI9WY_ZCggB_Nz6y7C53Y_G",
                Image_2 = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRxdY00Wd1A7cIw9kSEb_K3GQNx0jOdaloLwZb_spqnRTxYPnst",
                Text = "Welcome to NYU"
            });

            Example.ItemsSource = Items;
            Example.ItemTapped += Example_ItemTapped;
        }

        public void AddItems()
        {
            Items.Add(new ListViewItems { Name = "Name", Text ="Text" });
          
            
        }

        private async void Example_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            // More details about one item
            var detail = e.Item as ListViewItems;
            await Navigation.PushAsync(new Page1(detail.Name,detail.Text, detail.Image_2));
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            // Refresh the list
            List();
            Example.IsRefreshing = false;
        }

        void Handle_Delete(object sender, System.EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            var Deleteitem = (ListViewItems)menuItem.CommandParameter;
            Items.Remove(Deleteitem);
        }
      
    }

}
